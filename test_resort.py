import unittest
import resort
import skier
import trail


class TestResort(unittest.TestCase):
    resort = None

    @classmethod
    def setUpClass(cls):
        print('setUpClass()')
        cls.resort = resort.Resort().get()

        # create skiers and trails
        # used my roommates for examples they said it was ok
        # trails at sugarbush
        cls.t_sleeper = trail.Trail("Sleeper", 3907, 2, True)
        cls.t_jester = trail.Trail("Jester", 5174, 2, True)
        cls.t_waterfall = trail.Trail("Waterfall", 580, 3, True)
        cls.t_northstar = trail.Trail("Northstar", 2798, 1, True)
        cls.dean = skier.Skier("Dean", cls.t_sleeper, 2)
        cls.luke = skier.Skier("Luke", cls.t_jester, 3)
        cls.cassidy = skier.Skier("Cassidy", cls.t_waterfall, 4)
        cls.caleb = skier.Skier("Caleb", cls.t_northstar, 1)
        cls.resort.add_skier(cls.dean)
        cls.resort.add_skier(cls.luke)
        cls.resort.add_skier(cls.cassidy)
        cls.resort.add_skier(cls.caleb)
        cls.resort.add_trail(cls.t_sleeper)
        cls.resort.add_trail(cls.t_jester)
        cls.resort.add_trail(cls.t_waterfall)
        cls.resort.add_trail(cls.t_northstar)

    def setUp(self):
        print('setUp()')

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass()')

    def tearDown(self):
        print('tearDown()')
        self.resort.shutdown_resort()

    ######################################

    def test_is_favorite_trail_open_incorrect(self):
        # this test should fail its the incorrect one

        # check if trail is open
        # should return true but incorrect implementation will return false
        test = self.resort.is_favorite_trail_open_incorrect("Dean")
        self.assertFalse(test)

        # now close the trail
        self.t_sleeper.close()
        # check if trail is closed
        # should return false but won't
        test = self.resort.is_favorite_trail_open_incorrect("Dean")
        self.assertTrue(test)

        # open the trail and test with a skier that doesn't exist
        # now close the trail
        self.t_sleeper.open()
        test = self.resort.is_favorite_trail_open_incorrect("Not Dean")
        self.assertTrue(test)

    def test_is_favorite_trail_open_correct(self):
        # check if trail is open
        # should return true
        test = self.resort.is_favorite_trail_open("Dean")
        self.assertTrue(test)

        # check if false for a skier that doesn't exist
        test = self.resort.is_favorite_trail_open("Not Dean")
        self.assertFalse(test)

    def test_find_skier_pass(self):
        # find the first skier
        test = self.resort.find_skier_pass(0)
        self.assertTrue(test == self.dean)

        # attempt to find a skier that doesnt have a pass
        test = self.resort.find_skier_pass(100)
        self.assertTrue(test is None)

    def test_find_skier_name(self):
        # find luke
        test = self.resort.find_skier_name("Luke")
        self.assertTrue(test == self.luke)

        # attempt to find a skier that doesnt have a pass
        test = self.resort.find_skier_name("Not Luke")
        self.assertTrue(test is None)

    def test_is_open(self):
        # check if trail is open
        # should return true
        test = self.resort.is_open(self.t_sleeper)
        self.assertTrue(test)

        # close the trail and test again
        self.t_sleeper.close()
        test = self.resort.is_open(self.t_sleeper)
        self.assertFalse(test)

    def test_find_trail_name(self):
        # check if the resort has a trail that exists
        t = self.resort.find_trail_name("Sleeper")
        self.assertTrue(t == self.t_sleeper)

        # check if a resort doesn't have a trail
        # should return none
        t = self.resort.find_trail_name("Not Sleeper")
        self.assertTrue(t is None)

    def test_find_trail_difficulty(self):
        # find all the trails w a difficulty of 2
        # case w multiple trails
        trails = self.resort.find_trail_difficulty(2)
        self.assertTrue(trails == [self.t_sleeper, self.t_jester])

        # find all the trails w a difficulty of 2
        # case w one
        trails = self.resort.find_trail_difficulty(1)
        self.assertTrue(trails == [self.t_northstar])

        # find all the trails w a difficulty of 4
        # case w no trails
        trails = self.resort.find_trail_difficulty(4)
        self.assertTrue(trails == [])
