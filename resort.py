import skier
import trail


class Resort:
    s_resort = None

    @classmethod
    def get(self):
        if self.s_resort is None:
            self.s_resort = Resort()
        return self.s_resort

    def __init__(self):
        self.skiers = []
        self.trails = []

    def add_skier(self, s):
        self.skiers.append(s)

    def add_trail(self, t):
        self.trails.append(t)

    def shutdown_resort(self):
        self.skiers = []
        self.trails = []

    def get_skiers(self):
        return self.skiers

    def get_trails(self):
        return self.trails

    def find_skier_pass(self, pass_num):
        for s in self.skiers:
            if s.get_pass_num() == pass_num:
                return s
        return None

    def find_skier_name(self, name):
        for s in self.skiers:
            if s.get_name() == name:
                return s
        return None

    def is_open(self, t):
        return t.is_trail_open()

    def find_trail_name(self, trail_name):
        for t in self.trails:
            if t.get_name() == trail_name:
                return t
        return None

    def find_trail_difficulty(self, dif):
        trails_dif = []
        for t in self.trails:
            if t.get_difficulty() == dif:
                trails_dif.append(t)
        return trails_dif

    def is_favorite_trail_open(self, skier_name):
        s = self.find_skier_name(skier_name)

        if s is None:
            return False

        return s.get_favorite_run().is_trail_open()

    def is_favorite_trail_open_incorrect(self, skier_name):
        s = self.find_skier_name(skier_name)

        if s is None:
            return True

        return not (s.get_favorite_run().is_trail_open())

    def close_all(self):
        for t in self.trails:
            t.close()
