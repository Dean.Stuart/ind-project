import skier
import trail
import resort


def main():
    sleeper = trail.Trail("Sleeper Road", 2300, 2, True)
    exterminator = trail.Trail("Exterminator", 1950, 4, False)
    northstar = trail.Trail("Northstar", 2798, 1, True)

    dean = skier.Skier("Dean", sleeper, 2)
    james = skier.Skier("James", exterminator, 4)
    jessie = skier.Skier("Jessie", northstar, 3)

    sugarbush = resort.Resort()

    sugarbush.add_trail(sleeper)
    sugarbush.add_trail(exterminator)
    sugarbush.add_trail(northstar)
    sugarbush.add_skier(dean)
    sugarbush.add_skier(james)
    sugarbush.add_skier(jessie)

    print(dean.get_pass_num())

    print(sugarbush.find_skier_pass(0).get_name())

    print("Is " + dean.get_name() + "'s favorite trail open?: " + str(sugarbush.is_favorite_trail_open("Dean")))
    print(jessie.get_name() + "'s pass number: " + str(jessie.get_pass_num()))
    print("Can " + james.get_name() + "do Exterminator? " + str(james.can_do_trail(exterminator)))
    print("How about " + dean.get_name() + "? " + str(dean.can_do_trail(exterminator)))

    sugarbush.close_all()

    print("All trails have been closed")
    print("Is " + dean.get_name() + "'s favorite trail open?: " + str(sugarbush.is_favorite_trail_open("Dean")) + "\n")


main()
