# skier class

import trail


class Skier:
    next_pass_number = 0

    def __init__(self, name, favorite_run, skill_level):
        self.name = name
        self.pass_num = Skier.next_pass_number
        Skier.next_pass_number += 1
        self.favorite_run = favorite_run
        self.skill_level = skill_level

    def to_string(self):
        return self.name

    def get_name(self):
        return self.name

    def get_favorite_run(self):
        return self.favorite_run

    def get_pass_num(self):
        return self.pass_num

    def get_ski_level(self):
        return self.skill_level

    def __eq__(self, other):
        return self.name == other.name and self.pass_num == other.pass_num

    def can_do_trail(self, t):
        return self.skill_level > t.get_difficulty() or self.skill_level == t.get_difficulty()

