# trail class

class Trail:

    def __init__(self, name, length, difficulty, is_open):
        self.name = name
        self.length = length
        self.difficulty = difficulty
        self.is_open = is_open

    def to_string(self):
        return self.name + self.difficulty

    def get_name(self):
        return self.name

    def get_length(self):
        return self.length

    def get_difficulty(self):
        return self.difficulty

    def is_trail_open(self):
        return self.is_open

    def __eq__(self, other):
        return self.name == other.name and self.difficulty == other.difficulty

    def open(self):
        self.is_open = True

    def close(self):
        self.is_open = False


